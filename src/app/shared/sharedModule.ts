import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import {ThaiAgePipe} from '../pipes/to-thai-age.pipe';
import {ThaiDatePipe} from '../pipes/to-thai-date-pipe';
import  {ThaiTimePipe} from '../pipes/to-thai-time-pipe';
import {ThaiDateTimePipe} from '../pipes/to-thai-datetime-pipe';
@NgModule({
  declarations: [ ThaiAgePipe,ThaiDatePipe,ThaiTimePipe,ThaiDateTimePipe],
  imports: [
    CommonModule,
  ],
  // exports is required so you can access your component/pipe in other modules
  exports: [ThaiAgePipe,ThaiDatePipe,ThaiTimePipe,ThaiDateTimePipe]
})
export class SharedModule {}