import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { authGuard } from './core/guard/auth.guard';
import { adminGuard } from './core/guard/auth-admin.guard';
import { AppLayoutComponent } from './layout/app.layout.component';

const routes: Routes = [
    {
        path: '', component: AppLayoutComponent,
        children: [
           {
                path: '',
                data: { breadcrumb: 'List Ward' },
                canActivate: [authGuard],
                loadChildren: () => import('./components/apps/list-ward/list-ward.module').then(m => m.ListWardModule)
            },
            { path: 'list-patient', data: { breadcrumb: 'List Patient' }, loadChildren: () => import('./components/apps/list-patient/list-patient.module').then(m => m.ListPatientModule) },
            { path: 'doctor-order', data: { breadcrumb: 'Doctor Order' }, loadChildren: () => import('./components/apps/doctor-order/doctor-order.module').then(m => m.DoctorOrderModule) },
            { path: 'admission-note', data: { breadcrumb: 'Admission Note' }, loadChildren: () => import('./components/apps/admission-note/admission-note.module').then(m => m.AdmissionNoteModule) },
            { path: 'patient-info', data: { breadcrumb: 'Patient Info' }, loadChildren: () => import('./components/apps/patient-info/patient-info.module').then(m => m.PatientInfoModule) },
            { path: 'ekg', data: { breadcrumb: 'EKG' }, loadChildren: () => import('./components/apps/ekg/ekg.module').then(m => m.EkgModule) },
            { path: 'consult', data:{breadcrumb:'Consult'}, loadChildren:()=> import('./components/apps/consult/consult.module').then(m=> m.ConsultModule)},
            { path: 'lab', data:{breadcrumb:'LAB'}, loadChildren:()=> import('./components/apps/lab/lab.module').then(m=> m.LabModule)}
        ]
    },
    // { path: 'auth', data: { breadcrumb: 'Auth' }, loadChildren: () => import('./demo/components/auth/auth.module').then(m => m.AuthModule) },
    { path: 'login', data: { breadcrumb: 'Login' }, loadChildren: () => import('./components/auth/login/login.module').then(m => m.LoginModule) },
    { path: 'landing', loadChildren: () => import('./components/landing/landing.module').then(m => m.LandingModule) },
    { path: 'notfound', loadChildren: () => import('./components/notfound/notfound.module').then(m => m.NotfoundModule) },
    { path: '**', redirectTo: '/notfound' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
