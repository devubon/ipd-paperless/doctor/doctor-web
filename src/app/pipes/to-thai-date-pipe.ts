
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'thaidate'
})

export class ThaiDatePipe implements PipeTransform {
    transform(date: string, format: string): string {
        // console.log(date);

        const mydate = date;
        const dateObject = new Date(mydate);
        const localDated = dateObject.toLocaleString('th-TH', {
            timeZone: 'Asia/Bangkok',
            weekday: 'long',
            year: 'numeric',
            month: 'numeric',
            day: 'numeric',
            hour: 'numeric',
            minute: 'numeric',
        });
        // console.log(localDated); // Output: '2/29/2024, 5:00:00 PM' (formatted according to en-US locale)
        
        const dName = localDated.split(' ')[0]; // Output: วันจันทร์
        const d = localDated.split(' ')[1].split('/')[0]; // Output: 29
        const m = parseInt(localDated.split(' ')[1].split('/')[1]) - 1; // Output: 1 (for February)
        const y = parseInt(localDated.split(' ')[1].split('/')[2].split(',')[0]); // Output:  2567
        const h = localDated.split(' ')[2].split(':')[0]; // Output: 17
        const min = localDated.split(' ')[2].split(':')[1]; // Output: 00

        // console.log(year); // Output: 2024


        let data: string = date.toString();
        let returnDate: string;

        let shortThaiMonth = [
            'ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.',
            'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'
        ];
        let longThaiMonth = [
            'มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
            'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
        ];
        let inputDate = new Date(date);

        if (inputDate.toString() != 'Invalid Date' && data) {

            let outputDateFull = [
                dName,
                'ที่ ' + d,
                'เดือน ' + longThaiMonth[m],
                'พ.ศ. ' + y
            ];
            let outputDateShort = [
                d,
                shortThaiMonth[m],
                y.toString().substring(2, 4)
            ];
            let outputDateMedium = [
                d,         
                longThaiMonth[m],
                y
            ];
            let outputDateTimeShort = [
                d,
                shortThaiMonth[m],
                y.toString().substring(2, 4),
                ' เวลา ' + h + '.' + min + ' น.'
            ];
            let outputDateTimeMedium = [
                d,
                longThaiMonth[m],
                y,
                ' เวลา ' + h + '.' + min + ' น.'
            ];
            let outputDateTimeFull = [
                dName,
                'ที่ ' + d,
                'เดือน ' + longThaiMonth[m],
                'พ.ศ. ' + y,
                ' เวลา ' + h + '.' + min + ' น.'
            ];
            returnDate = outputDateMedium.join(" ");
            if (format == 'full') {
                returnDate = outputDateFull.join(" ");
            }
            if (format == 'medium') {
                returnDate = outputDateMedium.join(" ");
            }
            if (format == 'short') {
                returnDate = outputDateShort.join(" ");
            }
            if (format == 'datetime') {
                returnDate = outputDateTimeShort.join(" ");
            }
            if (format == 'datetimemedium') {
                returnDate = outputDateTimeMedium.join(" ");
            }
            if (format == 'datetimefull') {
                returnDate = outputDateTimeFull.join(" ");
            }
        } else {
            returnDate = data;
        }
        return returnDate;
    }
}