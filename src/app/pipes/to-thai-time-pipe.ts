
import { Pipe, PipeTransform } from '@angular/core';
 
@Pipe({
    name: 'thaitime'
})

export class ThaiTimePipe implements PipeTransform {
  transform(time: string): string {
    let returnTime : string;
    let timesplit = time.split(':');
    returnTime = timesplit[0] + '.' + timesplit[1] + ' น.';

    return returnTime;
  }
}