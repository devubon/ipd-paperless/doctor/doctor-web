import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router } from '@angular/router';
import { LabService } from './lab-service';
import { MenuItem, MessageService } from 'primeng/api';
import { TreeNode } from 'primeng/api';
const _ = require('lodash');

@Component({
  selector: 'app-lab',
  templateUrl: './lab.component.html',
  styleUrls: ['./lab.component.scss'],
  providers: [MessageService],
})
export class LabComponent implements OnInit {
  queryParamsData: any;
  patientList: any = [];
  patientLists: any;
  labtList: any;
  an: any;
  hn: any;
  title: any;
  fname: any;
  lname: any;
  gender: any;
  age: any;
  phone: any;
  list: any = [];
  items: any[] = [];
  listlab: any = [];
  listOrder: any = [];
  datas: any = [];
  activeIndex: number = 0;
  speedDialitems: MenuItem[] = [];
  files!: TreeNode[];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private spinner: NgxSpinnerService,
    // private renderer: Renderer2,
    // private messageService:MessageService,
    private labService: LabService,
    private messageService: MessageService
  ) {
    let jsonString: any =
      this.activatedRoute.snapshot.queryParamMap.get('data');
    const jsonObject = JSON.parse(jsonString);
    this.queryParamsData = jsonObject;
    console.log(this.queryParamsData);
  }
  async ngOnInit() {
    await this.getPatient();
    this.buttonSpeedDial();
    await this.getListlabdata();
  }
  // changeItem(i: number) {
  //     this.activeIndex = i;

  // }
  changeItem(i: number) {
    this.messageService.clear();
    if (this.listlab[i].questions[0]?.header) { // ตรวจสอบว่าข้อมูลใน listlab ที่ index i มี property label หรือไม่
      this.activeIndex = i;
    } else {
      this.activeIndex = i;
      this.messageService.add({
        severity: 'warn',
        summary: 'ไม่มีผลแลบ',
        detail: 'ผลแลบยังไม่ออกครับ',
      });
    }
  }

  //ข้อมูลคนป่วย
  async getPatient() {
    // console.log('getPatient');
    try {
      const response = await this.labService.getPatientInfo(
        this.queryParamsData
      );
      // console.log(response);

      // const data: any = response.data;

      this.patientList = response.data.data;
      this.patientLists = response.data.pateint;
      // console.log(this.patientList);

      this.hn = this.patientList.hn;
      this.an = this.patientList.an;
      this.title = this.patientList.patient.title;
      this.fname = this.patientList.patient.fname;
      this.lname = this.patientList.patient.lname;
      this.gender = this.patientList.patient.gender;
      this.age = this.patientList.patient.age;
      this.phone = this.patientList.phone;

      console.log('an : ', this.an);
      // if (this.an != null || this.an != undefined) {
      //     this.getListlabdata();
      // }
    } catch (error: any) {
      console.log(error);
    }
  }

  async processFilteredData(filteredData: any) {
    let treeLab: any[] = [];
    let i = 0;
    for (const doctorOrder of filteredData) {
      i++;
      let k = 0;
      let children: any[] = [];
      for (const order of doctorOrder.order) {
        k++;
        try {
          const results = await this.labService.getListlabbycode(this.an, order.code);
          // console.log(results.data);

          let grandChildren: any[] = [];
          for (const result of results.data) {
            grandChildren.push({
              key: `${i}-${k}-${grandChildren.length}`,
              label: `${result.lab_test_name}: ${result.lab_test_result}`,
              icon: 'pi pi-fw pi-file',
              data: result.lab_test_name
            });
          }

          children.push({
            key: `${i}-${k}`,
            label: order.item_name,
            data: order.item_name,
            icon: 'pi pi-fw pi-cog',
            children: grandChildren
          });

        } catch (error) {
          console.log(error);
        }
      }

      treeLab.push({
        key: i.toString(),
        label: doctorOrder.doctor_order_date,
        data: doctorOrder.doctor_order_date,
        icon: 'pi pi-fw pi-inbox',
        children: children
      });
    }
    this.files = treeLab;
    // console.log(treeLab);
    return treeLab;
  }
  expandAll() {
    this.files.forEach((node) => {
      this.expandRecursive(node, true);
    });
  }

  collapseAll() {
    this.files.forEach((node) => {
      this.expandRecursive(node, false);
    });
  }

  private expandRecursive(node: TreeNode, isExpand: boolean) {
    node.expanded = isExpand;
    if (node.children) {
      node.children.forEach((childNode) => {
        this.expandRecursive(childNode, isExpand);
      });
    }
  }
  async getListlabdata() {
    try {
      const response = await this.labService.ListOrder(this.queryParamsData);
      // console.log('ListOrder  : ', response);
    

      const filteredData = _.chain(response.data.data)
        .map((order: any) => {
          const filteredOrders = _.filter(order.order, { item_type_id: 1 });
          if (!_.isEmpty(filteredOrders)) {
            return { ...order, order: filteredOrders };
          }
        })
        .compact() // ลบรายการที่เป็น undefined หรือ null ออก
        .value();

      // console.log(filteredData);
   

      try {
        await this.processFilteredData(filteredData);
      } catch (error) {
        console.error('Error processing filtered data:', error);
      }

    } catch (error) {
      console.log(error);
    }

  }

 
  async getListlabbycode(items: any[]) {
    console.log(items);
    try {
      let i = 1;

      for await (const r of items) {


        if (r.item_type_id === 1) {
          // console.log(i++);item_name
          console.log(r.item_name);
          console.log('getResultLab');
          let results: any = await this.labService.getListlabbycode('66000124', r.item_id);
          console.log('results:', results);
        } else {
          console.log('Not lab');
        }
      }
    } catch (error) {
      console.log(error);
      return;
    }
  }

  backlistPatient() {
    this.router.navigate(['/list-patient']);
  }

  buttonSpeedDial() {
    this.speedDialitems = [
      {
        icon: 'pi pi-arrow-left',
        routerLink: ['/list-patient'],
        tooltipOptions: {
          tooltipLabel: 'ย้อนกลับ',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-user-doctor',
        command: () => {
          this.navigateDoctorOrder()
        },
        tooltipOptions: {
          tooltipLabel: 'Doctor Order',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-circle-info',
        command: () => {
          this.navigatePatientInfo()
        },
        tooltipOptions: {
          tooltipLabel: 'Patient Info',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-comment-medical',
        command: () => {
          this.navigateAdmisstionNote()
        },
        tooltipOptions: {
          tooltipLabel: 'Admission Note',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-heart-pulse',
        command: () => {
          this.navigateEkg()
        },
        tooltipOptions: {
          tooltipLabel: 'EKG',
          tooltipPosition: 'bottom'
        },
      },
      {
        icon: 'fa-solid fa-phone-volume',
        command: () => {
          this.navigateConsult()
        },
        tooltipOptions: {
          tooltipLabel: 'Consult',
          tooltipPosition: 'bottom'
        },
      },
    ];
  }

  navigatePatientInfo() {
    let jsonString = JSON.stringify(this.queryParamsData);
    console.log(jsonString);
    this.router.navigate(['/patient-info'], { queryParams: { data: jsonString } });

  }

  navigateDoctorOrder() {
    let jsonString = JSON.stringify(this.queryParamsData);
    console.log('admit id:', jsonString);
    this.router.navigate(['/doctor-order'], { queryParams: { data: jsonString } });

  }

  navigateAdmisstionNote() {
    let jsonString = JSON.stringify(this.queryParamsData);
    console.log(jsonString);
    this.router.navigate(['/admission-note'], { queryParams: { data: jsonString } });

  }

  navigateConsult() {
    let jsonString = JSON.stringify(this.queryParamsData);
    console.log(jsonString);
    this.router.navigate(['/consult'], { queryParams: { data: jsonString } });

  }

  navigateEkg() {
    let jsonString = JSON.stringify(this.queryParamsData);
    console.log(jsonString);
    this.router.navigate(['/ekg'], { queryParams: { data: jsonString } });



  }
  //   collapseAll() {
  //     this.files.forEach((node) => {
  //         this.expandRecursive(node, false);
  //     });
  // }

  // private expandRecursive(node: TreeNode, isExpand: boolean) {
  //     node.expanded = isExpand;
  //     if (node.children) {
  //         node.children.forEach((childNode) => {
  //             this.expandRecursive(childNode, isExpand);
  //         });
  //     }
  // }

}
