

import { AfterViewInit, Component, ElementRef, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef } from '@angular/core';
import {
  CanvasWhiteboardComponent,
  CanvasWhiteboardService,
  CanvasWhiteboardUpdate,
  CanvasWhiteboardOptions,
  CanvasWhiteboardShapeService, CircleShape, SmileyShape, StarShape, LineShape, RectangleShape
} from 'ng2-canvas-whiteboard';
import { MessageService } from 'primeng/api';
import { AdmisstionNoteService } from '../admission-note-service';
import { Event } from '@angular/router';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { MinioService } from './minio-service';


@Component({
  selector: 'app-anatomy-drawing',
  templateUrl: './anatomy-drawing.component.html',
  viewProviders: [CanvasWhiteboardComponent],
  styleUrls: ['./anatomy-drawing.component.scss']
})
export class AnatomyDrawingComponent implements OnInit, AfterViewInit {
  @Input() parentData: any;
  @Output() dataEvent = new EventEmitter<string>();
  @ViewChild(CanvasWhiteboardComponent) canvasWhiteboardComponent!: CanvasWhiteboardComponent;
  loading:boolean=false;
  loadingImg:boolean=false;
  canvasHeight:string = '';

  canvasOptions: CanvasWhiteboardOptions = {};
  private canvas!: HTMLCanvasElement;
  @ViewChild('canvas') canvasRef?: ElementRef<HTMLCanvasElement>;

  private context: CanvasRenderingContext2D | null = null;
  private isDrawing: boolean = false;
  private lastX: number = 0;
  private lastY: number = 0;
  private img = new Image();
  // @ViewChild('fileInput') fileInput: any;
  // @Input() bufferData: { type: string, data: number[] } | undefined // Assuming you'll receive base64 string

  imageurl: any;
  imageUrls: string | ArrayBuffer | null = '';
  imageUrl: SafeUrl | undefined;

  constructor(
    private canvasWhiteboardService: CanvasWhiteboardService,
    private canvasWhiteboardShapeService: CanvasWhiteboardShapeService,
    private http: HttpClient,
    private cdRef: ChangeDetectorRef,
    private admisstionNoteService: AdmisstionNoteService,
    private messageService: MessageService,
    private sanitizer: DomSanitizer,
    private MinioService: MinioService
  ) {
    this.canvasWhiteboardShapeService.unregisterShapes([CircleShape, SmileyShape, StarShape, LineShape, RectangleShape]);
  }
  ngOnInit(): void {
    // console.log(this.bufferData);
    // if (this.bufferData?.type == 'null') {
    //   this.imageurl = '';

    // } else {
    //   let imgBufferData: any = this.bufferData?.data;
    //   this.imageurl = String.fromCharCode.apply(String, imgBufferData);
    // }

    this.cdRef.detectChanges(); // Trigger change detection
    this.canvas = this.canvasWhiteboardComponent.canvas.nativeElement;
    console.log(this.parentData);
    this.getImageFromMinio();


  }
  showSuccess() {
    this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Message Content' });
}
  ngAfterViewInit(): void {
    console.log('ngAfterViewInit');

    // this.cdRef.detectChanges(); // Trigger change detection
    // this.canvas = this.canvasWhiteboardComponent.canvas.nativeElement;

    // Load your image

    // this.img.onload = () => {
    //   // Draw the image onto the canvas if context is not null
    //   const context = this.canvas.getContext('2d');
    //   if (context) {
    //     context.drawImage(this.img, 0, 0, this.canvas.width, this.canvas.height);
    //   } else {
    //     console.error('Canvas context is null.');
    //   }
    // };

    // if (this.imageUrl) {
    //   this.img.src = this.imageUrl as string;
    // } else {
    //   this.img.src = '../../../../assets/images/body.jpg'; // Default background image
    // }
   
    // this.getImageFromMinio();




  }
  async getImageFromMinio() {
    if (this.parentData.file_name) {
      const bucketName = 'uploads';
      const objectKey = this.parentData.file_name; //1717484380148
      const signedUrl = this.MinioService.getSignedUrl(bucketName, objectKey);
      try {
        const blob = await this.MinioService.getImage(signedUrl);
        const objectURL = URL.createObjectURL(blob);
        this.imageUrl = this.sanitizer.bypassSecurityTrustUrl(objectURL);
        this.img.src = objectURL;
        this.updateCanvasWithImage(objectURL);
      } catch (error) {
        console.error('Error fetching image from Minio:', error);
      }
    } else {
      this.img.src = '../../../../assets/images/body.jpg'; // Default background image 1718650744134

    }

  }
  updateCanvasWithImage(imageSrc: string) {
    const context = this.canvas.getContext('2d');
    const image = new Image();
    image.onload = () => {
      
      // Set the canvas dimensions
      this.canvas.width = image.width;
      this.canvas.height = image.height;
      this.canvasHeight = image.height+'px';
      console.log('canvas height',this.canvasHeight);
      // Draw the image on the canvas
      context?.drawImage(image, 0, 0, image.width, image.height);
    };
    image.src = imageSrc;
    this.loadingImg = true;
  }



  onCanvasClear() {
    console.log('onCanvasClear');

    // Draw the image onto the canvas if context is not null
    const context = this.canvas.getContext('2d');
    if (context) {
      context.drawImage(this.img, 0, 0, this.canvas.width, this.canvas.height);
    } else {
      console.error('Canvas context is null.');
    }

    this.img.src = '../../../../assets/images/body.jpg'; // Replace with your background image path

    const canvas = this.canvasRef?.nativeElement;
    if (canvas) {
      this.context = canvas.getContext('2d');
      this.setupEventListeners(canvas);
    }

  }
  onCanvasUndo(e: Event) {
    this.img.src = '../../../../assets/images/body.jpg'; // Replace with your background image path

    const canvas = this.canvasRef?.nativeElement;
    if (canvas) {
      this.context = canvas.getContext('2d');
      this.setupEventListeners(canvas);
    }
  }






  public changeOptions(): void {
    this.canvasOptions = {
      ...this.canvasOptions,
      fillColorPickerEnabled: false,
      colorPickerEnabled: false
    };
  }
  private setupEventListeners(canvas: HTMLCanvasElement): void {
    canvas.addEventListener('mousedown', this.handleMouseDown.bind(this));
    canvas.addEventListener('mousemove', this.handleMouseMove.bind(this));
    canvas.addEventListener('mouseup', this.handleMouseUp.bind(this));
    canvas.addEventListener('mouseleave', this.handleMouseLeave.bind(this));

    canvas.addEventListener('touchstart', this.handleTouchStart.bind(this));
    canvas.addEventListener('touchmove', this.handleTouchMove.bind(this));
    canvas.addEventListener('touchend', this.handleTouchEnd.bind(this));

  }

  private handleMouseDown(e: MouseEvent): void {
    this.isDrawing = true;
    [this.lastX, this.lastY] = [e.offsetX, e.offsetY];
  }

  private handleMouseMove(e: MouseEvent): void {
    if (!this.context || !this.isDrawing) return;
    this.draw(e.offsetX, e.offsetY);
  }

  private handleMouseUp(): void {
    this.isDrawing = false;
  }

  private handleMouseLeave(): void {
    this.isDrawing = false;
  }

  private handleTouchStart(e: TouchEvent): void {
    const touch = e.touches[0];
    this.isDrawing = true;
    [this.lastX, this.lastY] = [touch.clientX, touch.clientY];
  }

  private handleTouchMove(e: TouchEvent): void {
    if (!this.context || !this.isDrawing) return;
    const touch = e.touches[0];
    this.draw(touch.clientX, touch.clientY);
  }

  private handleTouchEnd(): void {
    this.isDrawing = false;
  }

  private draw(x: number, y: number): void {
    if (!this.context) return;
    this.context.beginPath();
    this.context.moveTo(this.lastX, this.lastY);
    this.context.lineTo(x, y);
    this.context.stroke();
    [this.lastX, this.lastY] = [x, y];
  }

  // Method to handle save button click
  handleSave(event: String | Blob) {
    console.log(event);
    //event.preventDefault(); // Prevent the default download behavior
    setTimeout(() => {
      // const canvas = this.canvasRef?.nativeElement;
      // this.canvas = this.canvasWhiteboardComponent.canvas.nativeElement;
      // if (this.canvas) {
      //   const imageData = this.canvas.toDataURL('image/png');
      //   // Call the uploadImage() method with the base64 image data
      //   console.log(imageData);
      //   this.uploadImage(imageData);
      // } else {
      //   console.error('Canvas is undefined.');
      // }
      console.log('mdfkdjfdjf');
    });
  }



  ///////////////////////////////////////////////////////////////////////////////////////////////
  // @ViewChild('canvasWhiteboard') canvasWhiteboard: CanvasWhiteboardComponent;


  async saveImage() {
    this.loading = true;
    const base64Image = this.canvasWhiteboardComponent.generateCanvasDataUrl('image/png');
    const blob = this.base64ToBlob(base64Image.split(',')[1], 'image/png');
    const fileName = 'drawing.png';

    await this.uploadToMinio(blob, fileName).then(response => {
      console.log('Upload successful1', response);
      if (response) {
        // this.messageService.add({
        //   severity: 'success',
        //   summary: 'บันทึกสำเร็จ #',
        //   detail: 'รอสักครู่...',
        // });
        // this.messageService.add({ severity: 'success',  summary: 'บันทึกสำเร็จ #', detail: 'รอสักครู่...', life: 10000 });
      } else {
        console.error('Upload failed Save');
        // this.messageService.add({
        //   severity: 'error',
        //   summary: 'เกิดข้อผิดพลาด !',
        //   detail: 'ไม่สามารถบันทึกภาพได้',
        // });
        // this.messageService.add({ severity: 'error',  summary: 'เกิดข้อผิดพลาด !', detail: 'ไม่สามารถบันทึกภาพได้', life: 30000 });

      }

    }).catch(error => {
      this.loading = false;
      console.error('Upload failed', error);
      // this.messageService.add({
      //   severity: 'error',
      //   summary: 'เกิดข้อผิดพลาด !',
      //   detail: 'ไม่สามารถบันทึกภาพได้',
      // });
      this.messageService.add({ severity: 'error',  summary: 'เกิดข้อผิดพลาด !', detail: 'ไม่สามารถบันทึกภาพได้', life: 30000 });
    });
  }
  async uploadToMinio(file: Blob, fileName: string): Promise<any> {
    const formData = new FormData();
    formData.append('file', file, fileName);
    await this.MinioService.uploadFile(file, fileName).then(response => {
      console.log('Upload successful2', response);
      if (response.message) {
        this.messageService.add({ severity: 'success',  summary: 'บันทึกสำเร็จ #', detail: 'รอสักครู่...', life: 10000 });

         this.saveAdmissionNote(response.message.key);
      }
    }).catch(error => {
      this.loading = false;

      console.error('Upload failed', error);
      this.messageService.add({ severity: 'error',  summary: 'เกิดข้อผิดพลาด !', detail: 'ไม่สามารถบันทึกภาพได้', life: 30000 });

    });
  }

  base64ToBlob(base64: string, mimeType: string): Blob {
    const byteCharacters = atob(base64);
    const byteNumbers = new Array(byteCharacters.length);

    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }

    const byteArray = new Uint8Array(byteNumbers);
    return new Blob([byteArray], { type: mimeType });
  }
  async saveAdmissionNote(key: string) {
    try {
      let u: any = sessionStorage.getItem('userData');
      let uid = JSON.parse(u);
      let usid = uid.user_id;

      const currentDate = new Date();
      const year = currentDate.getFullYear();
      const month = String(currentDate.getMonth() + 1).padStart(2, '0'); // Month is zero-based, so add 1
      const day = String(currentDate.getDate()).padStart(2, '0');
      const formattedDate = `${year}-${month}-${day}`;
      let data = {
        admission_note: [{
          id: this.parentData.id,
          admit_id: this.parentData.id,
          file_name: key,
          modify_by: usid,
          modify_date: formattedDate

        }]
      }
      console.log(data);
      let rs = await this.admisstionNoteService.saveAdmissionNote(data);
      console.log(rs);
      this.loading = false;

    } catch (error) {
      this.loading = false;
      console.log(error);
      this.messageService.add({ severity: 'error',  summary: 'เกิดข้อผิดพลาด !', detail: 'ไม่สามารถบันทึกภาพได้', life: 30000 });

    }
  }
}



