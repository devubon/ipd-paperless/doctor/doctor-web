import { Injectable } from '@angular/core';
import axios from 'axios';


//import { Readable } from 'stream'; // นำเข้าจาก stream ของ Node.js

@Injectable({
  providedIn: 'root'
})
export class MinioService {
  private minioClient: Minio.Client;
  private serverUrl = 'http://209.15.114.75:40019'; // URL ของเซิร์ฟเวอร์ Minio
  private axiosInstance = axios.create({
      baseURL: `${this.serverUrl}`
  })
  private accessKey = 'SBb0SDieMAzDBld0by2q';
  private secretKey = 'Yso23JCi6WGcQxpzRNmgsWGengsWY91EV8T3b8XS';

  constructor() {
    this.minioClient = new Minio.Client({
      endPoint: 'YOUR_MINIO_ENDPOINT',
      port: 9000,
      useSSL: true,
      accessKey: 'YOUR_ACCESS_KEY',
      secretKey: 'YOUR_SECRET_KEY'
    });
    ///////create connect by axios//////
    this.axiosInstance.interceptors.request.use(config => {
        const token = sessionStorage.getItem('token');
        if (token) {
            config.headers['Authorization'] = `Bearer ${token}`;
        }
        return config;
    });

    this.axiosInstance.interceptors.response.use(response => {
        return response;
    }, error => {
        return Promise.reject(error);
    });

  }
  async uploadFile(file: Blob, fileName: string): Promise<any> {
    const formData = new FormData();
    formData.append('file', file, fileName);

    try {
        const response = await this.axiosInstance.post(`${this.serverUrl}/api/file/upload`, formData);
        return response.data;
    } catch (error) {
        throw error;
    }
}

  async getFile(bucketName: string, objectKey: string): Promise<Blob> {
    try {
      const dataStream = await this.minioClient.getObject(bucketName, objectKey);
      const chunks: any[] = [];

      return new Promise((resolve, reject) => {
        dataStream.on('data', (chunk:any) => {
          chunks.push(chunk);
        });

        dataStream.on('end', () => {
          const blob = new Blob(chunks);
          resolve(blob);
        });

        dataStream.on('error', (err:any) => {
          reject(err);
        });
      });
    } catch (err) {
      console.log(err);
      throw err;
    }
  }
}
