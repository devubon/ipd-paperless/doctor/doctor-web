import { Injectable } from '@angular/core';
import axios from 'axios';


import * as AWS from 'aws-sdk';
// import AWS object without services
// import AWS from 'aws-sdk/global';
// import individual service
// import S3 from 'aws-sdk/clients/s3';




@Injectable({
    providedIn: 'root'
})
export class MinioService {

    private serverUrl = 'http://209.15.114.75:40019'; // URL ของเซิร์ฟเวอร์ Minio
    private axiosInstance = axios.create({
        baseURL: `${this.serverUrl}`
    });
    private accessKey = 'SBb0SDieMAzDBld0by2q';
    private secretKey = 'Yso23JCi6WGcQxpzRNmgsWGengsWY91EV8T3b8XS';
    private s3: AWS.S3;

    constructor() {

  

        this.axiosInstance.interceptors.request.use(config => {
            const token = sessionStorage.getItem('token');
            if (token) {
                config.headers['Authorization'] = `Bearer ${token}`;
            }
            return config;
        });

        this.axiosInstance.interceptors.response.use(response => {
            return response;
        }, error => {
            return Promise.reject(error);
        });

        this.s3 = new AWS.S3({
            accessKeyId: 'SBb0SDieMAzDBld0by2q',
            secretAccessKey: 'Yso23JCi6WGcQxpzRNmgsWGengsWY91EV8T3b8XS',
            endpoint: 'http://209.15.114.75:40019', // เช่น 'http://209.15.114.75:40019'
            s3ForcePathStyle: true, // จำเป็นสำหรับ Minio
            signatureVersion: 'v4'
        });
     
        window.AWS = AWS;

    }
    getSignedUrl(bucketName: string, objectKey: string): string {
        const params = {
          Bucket: bucketName,
          Key: objectKey,
          Expires: 60 // URL valid for 60 seconds
        };
        return this.s3.getSignedUrl('getObject', params);
      }

      async getImage(url: string): Promise<Blob> {
        try {
          const response = await axios.get(url, {
            responseType: 'blob'
          });
          return response.data;
        } catch (error) {
          console.error('Error fetching image from Minio:', error);
          throw error;
        }
      }

    async uploadFile(file: Blob, fileName: string): Promise<any> {
        const formData = new FormData();
        formData.append('file', file, fileName);

        try {
            const response = await this.axiosInstance.post(`${this.serverUrl}/api/file/upload`, formData);
            return response.data;
        } catch (error) {
            throw error;
        }
    }
    async updatedFile(file: Blob, fileName: string): Promise<any> {
        const formData = new FormData();
        formData.append('file', file, fileName);

        try {
            const response = await this.axiosInstance.put(`${this.serverUrl}/api/file/upload`, formData);
            return response.data;
        } catch (error) {
            throw error;
        }
    }







}
